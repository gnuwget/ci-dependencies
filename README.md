# Wget Dependency Check

The purpose of this repository is to ensure that GNU Wget can build against the
latest versions of its dependent libraries.

This repository houses only a single script, `build-all.sh`. This script is
executed using a scheduled pipeline and it will attempt to rebuild all the
dependencies _if_ they have changed, and then build Wget and Wget2 against
those libraries.
