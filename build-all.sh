#!/usr/bin/env bash

# This a simple shell script to go along with the ArchLinux Docker image for
# testing

set -e
set -o pipefail
set -u

cd /tmp

# The order is important. Libraries that come after may depend on the one
# before
libraries=('libunistring-git' 'libidn2-git' 'libpsl-git' 'gnutls-git' 'nghttp2-git' 'libbrotli-git')
projects=('wget-git' 'wget2-git')

for lib in "${libraries[@]}" "${projects[@]}"; do
	pushd "$lib"
	# The gitlab runner may be root or may not have sudo privileges.
	# But our builduser does
	sudo -u builduser makepkg -si --noconfirm
	popd
done
